#!/bin/bash

echo 'Welcome to Cisco Log Retriever!'
echo 'Please ensure you are connected to the Cisco Console before proceeding.'

# return CPU utilization for the last 60 seconds, 60 minutes, and 72 hours
show processes cpu history > switch_logs.csv

# return the current the CPU utilization and which IOS processes are using the most CPU time
show processes cpu sorted 5sec > switch_logs.csv

# return the the packet count per CPU receive queue
show controllers cpu-interface > switch_logs.csv

# return count of CPU packets discarded due to congestion
show platform port-asic stats drop > switch_logs.csv

# return count of IP packet types received by the switch
show ip traffic > switch_logs.csv