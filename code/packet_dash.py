# dash specific imports
from dash.dependencies import Input, Output
import dash_auth
import dash
import dash_table
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import plotly.graph_objects as go
import cufflinks as cf
from plotly import graph_objs as go
import plotly.offline

# notebook and dataframe manipulation imports
import datetime as dt
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from sklearn.metrics import accuracy_score
from sklearn.cluster import KMeans
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

# import raw dataset into dataframe
df = pd.read_csv("~/data/switch_data.csv")
df_copy = df.copy(deep=True)

# remove columns with data redundancy or missing values
df = df.drop('sw_forwarding', 1)
df = df.drop('cpu_utilization_total', 1)
df = df.drop('dropped_packets', 1)
df = df.drop('dropped_packets_tcp', 1)

# modify top_system_process column
df = df.replace({'ARP Input': 1, 'IP Input': 0})
df = df.rename(columns={"top_system_process": "arp_top_system_process"})

# convert object to datetime
df['timestamp'] = pd.to_datetime(df['timestamp'])
# , format='%Y-%m-%d %H:%M:%S'

# convert datetime to hour
df['timestamp'] = df['timestamp'].dt.hour

# add column for network degradation
df.insert(5, 'network_degraded', int)

# populate network_degraded column
df.loc[df['packet_drop_rate'] <= 5, 'network_degraded'] = 0
df.loc[df['packet_drop_rate'] > 5, 'network_degraded'] = 1

# enforce integer data type
df['network_degraded'] = df.network_degraded.astype(int)

cf.go_offline()
cf.set_config_file(offline=False, world_readable=True)

# set app username and password
CREDENTIALS = [['username', 'password']]
                                                                      
# load external stylesheets
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(external_stylesheets=external_stylesheets)

# security measures
auth = dash_auth.BasicAuth(app, CREDENTIALS)

app.title = 'Packet Drop Prediction'

# analytics
# generate scatter plot
scatter = px.scatter(df, x="cpu_utilization_interrupt", y="packet_drop_rate", title='cpu_utilization_interrupt vs packet_drop_rate')

# generate time series/line graph
line = px.line(df, x='timestamp', y='packet_drop_rate', title='packet_drop_rate and cpu performance vs hour_of_day')
line.update_layout(xaxis_title='hour_of_day (24hr)',
                   yaxis_title='%')
line.add_trace(go.Scatter(x=df['timestamp'], y=df['cpu_utilization_interrupt'],
                    mode='lines',
                    name='cpu_utilization_interrupt'))
line.add_trace(go.Scatter(x=df['timestamp'], y=df_copy['cpu_utilization_total'],
                    mode='lines',
                    name='cpu_utilization_total'))
# generate boxplot
boxplot = px.box(df, x="arp_top_system_process", y="packet_drop_rate", title='arp_top_system_process vs packet_drop_rate')

# generate confusion matrix
con_matrix = df.corr().iplot(kind='heatmap', 
                              colorscale="Reds", 
                              title="confusion matrix", 
                              theme='white', 
                              asFigure=True)

app.layout = html.Div([
    
    # header
    html.Div([
        html.H1('Packet Drop Prediction'),
        html.Div('Predict network degradation using packet drop rate and CPU utilization'),
        html.Div('Each graph is interactive. Drag and select to zoom in.')
    ], className = "header"),
    
    # graphs
    html.Div([
        html.Div([
            dcc.Graph(id='scatter_plot', figure=scatter)
        ], className = "graph_one"),
        
        html.Div([
            dcc.Graph(id='line_graph', figure=line)
        ], className = "graph_two"),
        
        html.Div([
            dcc.Graph(id='box_plot', figure=boxplot)
        ], className = "graph_three"),
        
        html.Div([
            dcc.Graph(id='confusion_matrix', figure = con_matrix)
        ], className = "graph four")
    ], className="row"),
    
    # real-time queries
    html.Div([
        # manipulate data table
        html.Div([
           html.H6('Editable Data Table'),
           dash_table.DataTable(
               id='Table',
               columns = [{'name': i, 'id': i} for i in df.columns],
               data=df.to_dict('records'),
               style_table={
                   'height': '300px',
                   'overflowX': 'auto'
               },
               style_header={
                   'backgroundColor': 'rgb(30, 30, 30)',
                   'fontWeight': 'bold'
               },
               style_cell={
                   'backgroundColor': 'rgb(50, 50, 50)',
                   'color': 'white',
                   'height': 'auto',
                   'width': 'auto'
               },
               editable=True,
               sort_action='native',
               row_selectable='multiple',
               selected_rows=[0],
               fixed_rows={'headers': True, 'data': 0},
           )
        ], className = 'data_table'),
        
#        # linked subject bar chart
#       html.Div([
#            dcc.Graph(id='linked_histogram'), 
#        ], className = 'graph five'),
        
    ], className = 'row')   
])


#@app.callback(
#    Output('linked_histogram', 'figure'),
#    [Input('Table', 'selected_rows')])

#def update_graph(sr):
#    
#    d = df.iloc[sr]
#    d = d.reset_index(drop=True)
#    
#    return d[['timestamp', 
#              'cpu_utilization_interrupt',
#              'packet_drop_rate', 
#              'arp_top_system_process', 
#              'dropped_packets_udp',
#              'network_degraded'
#             ]].iplot(kind='bar', # alt kind='area' or kind='scatter' or kind='bar'
#                            barmode='grouped', 
#                            theme='white', 
#                            title = 'Click feature names on the right to add or remove to customize comparisons.',
#                            fill=True , 
#                           asFigure=True)


# sign into app using username and password
if __name__ == '__main__':
    app.run_server()